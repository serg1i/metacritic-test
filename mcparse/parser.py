#!/usr/local/bin/python3

import logging
import time

import requests
from bs4 import BeautifulSoup


class McParser(object):

    def __init__(self):
        self.url = 'http://www.metacritic.com/game/playstation-4'
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'}
        self.last_update_ts = 0
        self.mc_state = []

    def refresh_state(self):
        resp = requests.get(self.url, headers=self.headers)
        if resp.status_code != 200:
            SystemExit("oops received non-200 http code from Metacritic")
        else:
            self.last_update_ts = time.time()
            logging.info("Parsed Metacritic, timestamp: {0}".format(
                self.last_update_ts))

        t = BeautifulSoup(resp.content, "html.parser").find(
            'ol', {'class': 'list_products list_product_summaries'})
        p, s = t.find_all("div", "basic_stat"), t.select(".metascore_w")
        if len(p) != 0:
            self.mc_state.clear()
            for i in range(0, len(p)):
                self.mc_state.append(
                    {'title': p[i].text.strip(), 'score': int(s[i].text.strip())})

    def get_game(self, game_title):
        game = str(game_title).lower().strip()
        if time.time() - self.last_update_ts > 60:
            self.refresh_state()
        try:
            r = [i for i in self.mc_state if str(
                i["title"]).lower() == game][0]
            return r['title'], r['score']
        except LookupError:
            logging.warning(
                "Title not found in top 10: {0}".format(game_title))
        return (game_title, 'Not found')

    def get_all(self):
        if time.time() - self.last_update_ts > 60:
            self.refresh_state()
        return self.mc_state
