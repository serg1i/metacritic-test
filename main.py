#!/usr/local/bin/python3

import urllib
from flask import Flask, jsonify
from mcparse.parser import McParser

app = Flask(__name__)


@app.route('/games/<path:gametitle>')
def game(gametitle):
    g = urllib.parse.unquote(gametitle)
    r = McParser().get_game(g)
    return jsonify({'title': r[0], 'score': r[1]})


@app.route('/games')
def games_all():
    r = McParser().get_all()
    return jsonify(r)


def main():
    app.run(debug=False, port=8080)


if __name__ == '__main__':
    main()
