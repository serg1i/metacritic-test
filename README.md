# Metacritic Parser/REST API server

# Task:

Write an application in which can do the following:

1. Parse the HTML for the "Top Playstation 4 Games (By Metascore)" on Metacritic's PS4 page: `http://www.metacritic.com/game/playstation-4`. Expose a method which can return the parsed information as an array of JSON elements that looks like the following:

```json
[{
    "title": "XCOM: Enemy Within",
    "score": 88
}, {
    "title": "Assasin’s Creed IV: Black Flag",
    "score": 88
    }
]
```

2. Expose a REST API for retrieving top PS4 games. There should be 2 exposed methods:
 - A HTTP `GET` request a `/games` returns all top PS4 games on metacritic page
 - A HTTP `GET` request at `/games/TITLE_OF_GAME_GOES_HERE` returns JSON for a specific job that matches the corresponding job title.

 For example, an `HTTP GET` at `/games/Gran%20Turismo%206` should return an individual JSON object for Gran Turismo 6 like so: `{ "title": "Gran Turismo 6", "score": 81 }`

### Deliverables:

  - Provide the source-code, which satisfies 1 and 2 described above.
  - Provide "Readme" style documentation on how to run your code.
  - Provide unit tests to prove the correctness of your code

### Time tracking
I started working on this task Wednesday, and finished it Thursday evening. In general spent 4-5 hours, I think.

 - Spent approx. 1 hour looking into Metacritic site, their page structure, googling for API providers for metacritic. 
 - Another couple hours I spent reading about BeautifulSoup and
 Flask  (I never used any of them before, so concepts were a bit unusual for me)
 - Last 3 hours I was testing and writing this code. 
 - 30 minutes for Readme, unittest and git repo.

### Python 3

```
pip3 install -r requirements.txt
python3 main.py
```

### Send test requests

```
curl http://127.0.0.1:8080/games # returns top10 as json
curl http://127.0.0.1:8080/games/Undertale # returns score for game if it is in top 10
```

## Unittests

To run unittests:
```
python3 -m unittest discover -v
```
Following test were created
